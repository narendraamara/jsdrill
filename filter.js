function filters(elements,cb) {
    filteredArray =[]
    for(let ele=0;ele<elements.length;ele+=1) {
        if (cb(elements[ele])) {
            filteredArray.push(elements[ele]);
        }
    }return filteredArray
}
module.exports = filters